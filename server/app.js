const webpush = require('web-push');

console.log(webpush.generateVAPIDKeys());

const pushSubscription = {
    endpoint: 'endpoint',
    keys: {
      auth: 'authkey',
      p256dh: 'p256dh'
    }
  };
   
  webpush.sendNotification(pushSubscription, 'Your Push Payload Text');