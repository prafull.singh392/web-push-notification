importScripts('./ngsw-worker.js')

console.log('Prafull testing service worker');

self.addEventListener('push', event => {
    console.log('[Service Worker] Push Received.');
    console.log(`[Service Worker] Push had this data: "${event.data.text()}"`);
    
    const title = 'Push';
    const options = {
        body: 'Yay it works.',
        icon: 'images/icon.png',
        badge: 'images/badge.png'
    };
    
    event.waitUntil(self.registration.showNotification(title, options));
})