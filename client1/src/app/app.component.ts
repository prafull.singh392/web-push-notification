import { Component, OnInit } from '@angular/core';
import { SwPush, SwUpdate } from '@angular/service-worker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'service-worker';
  constructor(
    private readonly swUpdate: SwUpdate,
    private readonly swPush: SwPush) {
    this.checkServiceWorker();
    
  }
  ngOnInit(): void {
    this.pushSubscription();
  }

  checkServiceWorker = (): void => {
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(event => {
        console.log('current: ', event.current, ':available:', event.available);
        if (confirm('update available')) {
          this.swUpdate.activateUpdate().then(_ => location.reload());
        }
      });
    }
  }

  
  pushSubscription(): void {
    if (this.swPush.isEnabled) {
      this.swPush.requestSubscription({
        serverPublicKey: ''
      }).then(response => {
        // this response will contain endpoint,auth,p256dh that need to send to server
        console.log('response:  ', response);
      }).catch(error => {
        console.log('error:  ', error);
      });
    }
  }
}
